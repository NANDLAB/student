import java.util.Date;

/**
 * Represents a student by name, ID, weight and birthday.
 */
public class Student {
    private String firstname;
    private String lastname;
    private long studentId;
    private double weight;
    private Date birthday;

    /**
     * An enum with all keys which can be used for comparing or sorting.
     */
    public enum SortKey {
        /**
         * first name
         */
        FIRSTNAME,
        /**
         * last name
         */
        LASTNAME,
        /**
         * student ID
         */
        STUDENT_ID,
        /**
         * weight
         */
        WEIGHT,
        /**
         * birthday
         */
        BIRTHDAY}

    /**
     * Initializes a new Student with zero and null attributes. They can be set with setters later.
     */
    public Student() {
        this(null, null, 0, 0, new Date(0));
    }

    /**
     * Constructs a new Student with the specified attributes.
     * @param firstname the students first name
     * @param lastname  the students last name
     * @param studentId the student ID
     * @param weight    the students weight
     * @param birthday  the students birthday
     */
    public Student(String firstname, String lastname, long studentId, double weight, Date birthday) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.studentId = studentId;
        this.weight = weight;
        this.birthday = birthday;
    }

    /**
     * Compare two students by the key sortKey.
     * @param sortKey the key by which the students are compared
     * @param s1      student 1
     * @param s2      student 2
     * @return -1 if s1 &lt; s2; 0 if s1 = s2; 1 if s1 &gt; s2.
     */
    public static int compare(SortKey sortKey, Student s1, Student s2) {
        return switch (sortKey) {
            case FIRSTNAME -> s1.getFirstname().compareTo(s2.getFirstname());
            case LASTNAME -> s1.getLastname().compareTo(s2.getLastname());
            case STUDENT_ID -> Long.compare(s1.getStudentId(), s2.getStudentId());
            case WEIGHT -> Double.compare(s1.getWeight(), s2.getWeight());
            case BIRTHDAY -> s1.getBirthday().compareTo(s2.getBirthday());
        };
    }

    /**
     * Get students first name.
     * @return first name
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set students first name.
     * @param firstname new first name
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get students last name.
     * @return last name
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Set students last name.
     * @param lastname new last name
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Get students full name.
     * @return name in the format <q>First Last</q>.
     */
    public String getName() {
        return firstname + " " + lastname;
    }

    /**
     * Get student ID.
     * @return student ID
     */
    public long getStudentId() {
        return studentId;
    }

    /**
     * Set student ID.
     * @param studentId new student ID
     */
    private void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    /**
     * Get students weight.
     * @return students weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Set students weight.
     * @param weight new weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Get students birthday date.
     * @return birthday date
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Set students birthday date.
     * @param birthday date
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * Get a string representation of the student with all his attributes.
     * @return string with full name, student ID, weight and birthday separated by space
     */
    @Override
    public String toString() {
        return getName() + " " + studentId + " " + weight + " " + birthday;
    }
}
