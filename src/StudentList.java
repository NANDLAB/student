import java.util.ArrayList;

/**
 * List container for Student objects.
 * Students can be added, removed, searched and sorted.
 * All students should have unique IDs.
 */
public class StudentList {
    private final ArrayList<Student> students;

    /**
     * Initialize an empty student list.
     */
    public StudentList() {
        students = new ArrayList<>();
    }

    /**
     * Copy constructor.
     * @param studentList student list to copy
     */
    public StudentList(StudentList studentList) {
        this.students = new ArrayList<>(studentList.students);
    }

    /**
     * Get the size of the student list. Zero if empty.
     * @return size
     */
    public int size() {
        return students.size();
    }

    /**
     * Add a student.
     * If a student with the same ID already exists, the list stays unchanged.
     * @param student student to add
     * @return true if succeeded, false if a student with the same ID already exists
     */
    public boolean add(Student student) {
        if (containsId(student.getStudentId())) {
            return false;
        }
        else {
            students.add(student);
            return true;
        }
    }

    /**
     * Remove a student.
     * Find the student with the same ID in the list and remove him.
     * If the student is not found the list stays unchanged.
     * @param student student to remove
     * @return true if a student was removed, false otherwise
     */
    public boolean remove(Student student) {
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getStudentId() == student.getStudentId()) {
                students.remove(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Remove a student by position.
     * @param pos student position in the list
     * @return the student removed or null if the position is out of bounds (pos &lt; 0 or pos &ge; size())
     */
    public Student remove(int pos) {
        try {
            return students.remove(pos);
        }
        catch(IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Get student by position in the list.
     * @param pos position of the student in the list
     * @return the student at pos or null if pos &lt; 0 or pos &ge; size()
     */
    public Student get(int pos) {
        try {
            return students.get(pos);
        }
        catch(IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Find students by last name.
     * @param lastname last name to search
     * @return array of indices of students with the specified last name
     */
    public ArrayList<Integer> findLastname(String lastname) {
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getLastname().equals(lastname)) {
                ret.add(i);
            }
        }
        return ret;
    }

    /**
     * Find students by first name.
     * @param firstname first name to search
     * @return array of indices of students with the specified first name
     */
    public ArrayList<Integer> findFirstname(String firstname) {
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getFirstname().equals(firstname)) {
                ret.add(i);
            }
        }
        return ret;
    }

    /**
     * Find a student by ID.
     * @param studentId the students ID
     * @return the position of the student found or -1 if not found
     */
    int findStudentId(long studentId) {
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getStudentId() == studentId) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Check if the list contains a student with the specified ID.
     * @param studentId the students ID
     * @return a boolean indicating whether a student with studentID was found
     */
    private boolean containsId(final long studentId) {
        return findStudentId(studentId) >= 0;
    }

    /**
     * Get a string representation of the student list.
     * All students are in the same order as they were added, separated by newlines.
     * @return a string representation of the student list
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < students.size(); i++) {
            if (i > 0) { str.append("\n"); }
            str.append(students.get(i).toString());
        }
        return str.toString();
    }

    /**
     * Choose a pivot element between left and right indices (both inclusive)
     * @param left left index
     * @param right right index
     * @return pivot element index
     */
    static private int choosePivot(int left, int right) {
        return (left + right) / 2;
    }

    /**
     * Sort the list part between left and right (both inclusive) in ascending order using quicksort.
     * @param key key by which to sort
     * @param left left index
     * @param right right index
     */
    private void quicksort(Student.SortKey key, int left, int right) {
        if (left < right) {
            Student pivotValue = get(choosePivot(left, right));
            int i = left;
            int j = right;
            do {
                while (Student.compare(key, get(i), pivotValue) < 0) {
                    i++;
                }
                while (Student.compare(key, get(j), pivotValue) > 0) {
                    j--;
                }
                if (i <= j) {
                    Student tmp = get(i);
                    students.set(i, students.get(j));
                    students.set(j, tmp);
                    i++;
                    j--;
                }
            }
            while (i <= j);
            quicksort(key, left, j);
            quicksort(key, i, right);
        }
    }

    /**
     * Sort the list by key in ascending order.
     * @param key the key by which to sort.
     */
    public void sort(Student.SortKey key) {
        quicksort(key, 0, size()-1);
    }
}
