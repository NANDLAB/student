import java.util.ArrayList;
import java.util.Date;

public class Main {

    public static java.util.Date myDate(int year, int month, int day) {
        return new Date(year - 1900, month - 1, day);
    }

    public static void main(String[] args) {
        Student agent = new Student("Elsa", "Walter", 180002, 61.3, myDate(2000, 9, 2)); // Date.valueOf("2000-09-02"));
        Student siegfried = new Student("Siegfried", "Ullmann", 180001, 65.8, myDate(2001, 3, 24)); // Date.valueOf("2001-03-24"));
        Student ulyssa = new Student("Ulyssa", "Mai", 180003, 59.7, myDate(2002, 4, 17)); // Date.valueOf("2002-04-17"));
        System.out.println(siegfried);
        System.out.println(ulyssa);
        System.out.println();
        System.out.println("Full name before changes: " + agent.getName());
        System.out.println("Old object string: " + agent);
        System.out.println("Old firstname: " + agent.getFirstname());
        agent.setFirstname("Erna");
        System.out.println("New firstname: " + agent.getFirstname());
        System.out.println("Old lastname: " + agent.getLastname());
        agent.setLastname("Wegner");
        System.out.println("New lastname: " + agent.getLastname());
        System.out.println("Full name now: " + agent.getName());
        System.out.println("Student ID: " + agent.getStudentId());
        System.out.println("Old weight: " + agent.getWeight());
        agent.setWeight(60.5);
        System.out.println("New weight: " + agent.getWeight());
        System.out.println("Old birthday: " + agent.getBirthday());
        agent.setBirthday(myDate(2001, 1, 7));
        System.out.println("New birthday: " + agent.getBirthday());
        System.out.println("New object string: " + agent);

        System.out.println("\nCreating a list...");
        StudentList list1 = new StudentList();
        String duplicateName = "Eckstein";
        Student katharina = new Student("Katharina", "Beckenbauer", 11923, 56.23, myDate(2000, 4, 11));
        System.out.println(list1.add(katharina));
        System.out.println(list1.add(new Student("Erich", duplicateName, 18019, 63.38, myDate(1997, 10, 1))));
        System.out.println(list1.add(new Student("Minna", "Bach", 20121, 64.3, myDate(1998, 1, 26))));
        System.out.println(list1.add(new Student("Ava", duplicateName, 24109, 64.14, myDate(2001, 8, 1))));
        System.out.println(list1.add(new Student("Wenzeslaus", "Glöckner", 20193, 71.5, myDate(1999, 11, 4))));
        System.out.println(list1.add(new Student("Friedemann", "Hahn", 12845, 68.28, myDate(1997, 5, 2))));
        System.out.println(list1.add(new Student("Emma", "Krause", 11534, 58.51, myDate(2002, 4, 22))));
        System.out.println(list1.add(new Student("Heinrike", "Falk", 14879, 57.53, myDate(2002, 2, 2))));
        System.out.println(list1.add(new Student("Werther", "Vogts", 39154, 67.9, myDate(1997, 7, 19))));
        System.out.println(list1.add(new Student("Falk", "Pletscher", 13617, 73.62, myDate(2000, 2, 1))));
        System.out.println("Trying to add the same student a second time returns " + list1.add(katharina) + ".");

        System.out.println("\nResulting list1:\n" + list1);

        System.out.println("\nCopy constructing list2 from list1.");
        System.out.println("In the following, list1 will be modified.");
        System.out.println("If the copy constructor works as it should, list2 should stay unchanged.");
        StudentList list2 = new StudentList(list1);

        System.out.println("\nRemove not contained student: " + list1.remove(agent));
        System.out.println("Remove contained student: " + list1.remove(katharina));
        System.out.println("Remove second student by index: " + list1.remove(1));

        System.out.println("\nRemoving all " + duplicateName + "s from list1 by index...");
        ArrayList<Integer> indexList = list1.findLastname(duplicateName);
        for (Integer index : indexList) {
            System.out.println(list1.remove(index));
        }
        System.out.println("\nRemove by an out of bounds index returns " + list1.remove(12) + ".");

        System.out.println("\nModified list1:\n" + list1);
        System.out.println("\nlist2:\n" + list2);

        System.out.println("\nlist2 still contains the initial ten entries, so the copy constructor worked.");

        System.out.println();
        long id1 = 321;
        System.out.println("Find student with ID " + id1 + ": " + list1.findStudentId(id1));
        long id2 = 14879;
        System.out.println("Find student with ID " + id2 + ": " + list1.findStudentId(id2));

        System.out.println("\nSorting list2 by lastname...");
        list2.sort(Student.SortKey.LASTNAME);
        System.out.println(list2);
    }
}
